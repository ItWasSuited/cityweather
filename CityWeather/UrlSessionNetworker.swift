//
//  URLSessionNetworker.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 10/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

typealias NetworkerResult = Result<(Data, Int), Error>

protocol Networker {
    func sendGetRequest(url: URL, completion: @escaping (NetworkerResult) -> Void)
}

struct UrlSessionNetworker: Networker {
    
    func sendGetRequest(url: URL, completion: @escaping (NetworkerResult) -> Void) {
        let urlSession = URLSession.shared
        
        log.info("url: \(url)")
        let task = urlSession.dataTask(with: url) {
            data, response, error in
            if let error = error {
                log.info("Data task failed with error: \(error)")
                completion(.failure(error))
            } else {
                let httpUrlResponse = response as! HTTPURLResponse
                log.info("Data task succeed with status code: \(httpUrlResponse.statusCode)")
                completion(.success(data!, httpUrlResponse.statusCode))
            }
        }
        task.resume()
    }
}

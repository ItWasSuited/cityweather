//
//  MainController.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 12/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

protocol ViewControllerProvider {
    func citiesViewController() -> CitiesViewController
    func cityDetailViewController(detailModel: CityDetailViewModel) -> UIViewController
}

class MainController: UINavigationController {
    
    // These 2 cannot be let. 
    // Extrange issue: init(rootViewController:) calls internally init(nibName: bundle:), 
    // so we have to override it(otherwise it crashes saying MainController does not implement it).
    // If they would be let we would have to set them in init(nibName: bundle:), and that's impossible.
    private var viewControllerProvider: ViewControllerProvider!
    private var citiesViewController: CitiesViewController! 
    
    init(viewControllerProvider: ViewControllerProvider) {
        let citiesViewController = viewControllerProvider.citiesViewController()
        
        super.init(rootViewController: citiesViewController)
        
        self.viewControllerProvider = viewControllerProvider
        citiesViewController.didLoadCityDetailBlock = { [weak self] cityDetail in
            self?.show(cityDetail: cityDetail)
        }
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func show(cityDetail: CityDetailViewModel) {
        pushViewController(
            viewControllerProvider.cityDetailViewController(detailModel: cityDetail),
            animated: true
        )
    }

}

//
//  CachedResponse+CoreDataProperties.swift
//  
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension CachedResponse {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CachedResponse> {
        return NSFetchRequest<CachedResponse>(entityName: "CachedResponse");
    }

    @NSManaged public var cityName: String
    @NSManaged public var data: NSData
    @NSManaged public var date: NSDate

}

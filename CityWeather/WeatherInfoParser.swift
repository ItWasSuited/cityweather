//
//  WeatherInfoParser.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 10/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

enum WeatherInfoParserError {
    case invalidJson
    case unexpectedJson
}

struct WeatherInfoParser {
    
    func parse(data: Data) -> Result<WeatherInfo, WeatherInfoParserError> {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
            return .failure(.invalidJson)
        }
        
        guard let topLevelDict = json as? [String: Any] else {
            return .failure(.unexpectedJson)
        }
        
        let imageUrl: URL?
        let main: String?
        let description: String?
        if
            let weathers = topLevelDict["weather"] as? [[String: Any]],
            let weather = weathers.first
        {
            let icon = weather["icon"] as? String
            imageUrl = icon.flatMap { URL(string: "http://openweathermap.org/img/w/\($0).png") }
            main = weather["main"] as? String
            description = weather["description"] as? String
            
        } else {
            imageUrl = nil
            main = nil
            description = nil
        }
        
        let temperature: Measurement<UnitTemperature>?
        let humidity: Double?
        if let mainDict = topLevelDict["main"] as? [String: Any] {
            temperature = (mainDict["temp"] as? Double).map { Measurement<UnitTemperature>(value: $0, unit: .celsius) }
            humidity = mainDict["humidity"] as? Double
        } else {
            temperature = nil
            humidity = nil
        }
        
        let wind: Wind?
        if
            let windDict = topLevelDict["wind"] as? [String: Any],
            let speed = windDict["speed"] as? Double,
            let degrees = windDict["deg"] as? Double
        {
            wind = Wind(
                speed: Measurement<UnitSpeed>(value: speed, unit: .metersPerSecond),
                direction: WindDirection(degrees: degrees)
            )
        } else {
            wind = nil
        }
        
        return .success(WeatherInfo(
            imageUrl: imageUrl,
            temperature: temperature,
            main: main,
            description: description,
            humidity: humidity,
            wind: wind
        ))
    }
}

//
//  CitiesViewController.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 09/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit

private let cellId = "cellId"

class CitiesViewController: UITableViewController {
    
    private let viewModel: CitiesViewModel
    var didLoadCityDetailBlock: ((CityDetailViewModel) -> Void)?
    
    init(viewModel: CitiesViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        title = "Cities"
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cities.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)

        let city = viewModel.cities[indexPath.row]
        cell.textLabel?.text = city.name

        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = viewModel.cities[indexPath.row]
        
        viewModel.load(city: city) { [weak self] result in
            DispatchQueue.main.async {
                self?.handle(loadCityResult: result)
            }
        }
    }
 
    private func handle(loadCityResult: LoadCityResult) {
        switch loadCityResult {
        case .success(let cityDetail):
            didLoadCityDetailBlock!(cityDetail)
        case .failure:
            showErrorAlert()
        }
    }
    
    private func showErrorAlert() {
        let alertController = UIAlertController(title: "Error", message: "Could not load weather", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        present(alertController, animated: true, completion: nil)
    }

}

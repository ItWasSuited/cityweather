//
//  CityDetailViewModel.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 10/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

struct WeatherInfo {
    let imageUrl: URL?
    let temperature: Measurement<UnitTemperature>?
    let main: String?
    let description: String?
    let humidity: Double?
    let wind: Wind?
}

struct Wind {
    let speed: Measurement<UnitSpeed>
    let direction: WindDirection
}

enum WindDirection: String {
    case n, ne, e, se, s, sw, w, nw
}

extension WindDirection {
    
    init(degrees: Double) {
        switch degrees {
        case 0 ..< 22.5:
            self = .n
        case 22.5 ..< 67.5:
            self = .ne
        case 67.5 ..< 112.5:
            self = .e
        case 112.5 ..< 157.5:
            self = .se
        case 157.5 ..< 202.5:
            self = .s
        case 202.5 ..< 247.5:
            self = .sw
        case 247.5 ..< 292.5:
            self = .w
        case 292.5 ..< 337.5:
            self = .nw
        case 337.5 ..< 360:
            self = .n
        default:
            self = .n
        }
    }
}

struct CityDetailViewModel {
    let city: City
    let weatherInfo: WeatherInfo
}

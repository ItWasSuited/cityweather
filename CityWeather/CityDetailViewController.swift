//
//  CityDetailViewController.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 09/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit
import SDWebImage

private let formatter: MeasurementFormatter = {
    let formatter = MeasurementFormatter()
    formatter.unitOptions = .providedUnit
    return formatter
}()

class CityDetailViewController: UIViewController {
    
    private let viewModel: CityDetailViewModel
    
    required init(viewModel: CityDetailViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.city.name
        
        let weatherInfo = viewModel.weatherInfo
        
        imageView.sd_setImage(with: viewModel.weatherInfo.imageUrl)
        
        if let temperature = weatherInfo.temperature {
            temperatureLabel.text = String(format: "%@", formatter.string(from: temperature))
        } else {
            temperatureLabel.text = "?"
        }
        
        mainLabel.text = weatherInfo.main ?? ""
        
        descriptionLabel.text = weatherInfo.description ?? ""
        
        if let humidity = weatherInfo.humidity {
            humidityLabel.text = String(format: "%.2f %%", humidity)
        } else {
            humidityLabel.text = "?"
        }
        
        if let wind = weatherInfo.wind {
            windLabel.text = String(format: "%@ %@", wind.direction.rawValue.uppercased(), formatter.string(from: wind.speed))
        } else {
            windLabel.text = "None"
        }
    }

}

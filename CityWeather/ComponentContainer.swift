//
//  ComponentContainer.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 09/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import UIKit
import CoreData

class ComponentContainer: ViewControllerProvider {
    
    static let shared = ComponentContainer()
    
    func mainController() -> MainController {
        return MainController(viewControllerProvider: self)
    }
    
    func citiesViewController() -> CitiesViewController {
        return CitiesViewController(viewModel: citiesViewModel())
    }
    
    func cityDetailViewController(detailModel: CityDetailViewModel) -> UIViewController {
        return CityDetailViewController(viewModel: detailModel)
    }
    
    private let networker = UrlSessionNetworker()
    
    private let persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: Constants.weatherCacheModelName)
        container.loadPersistentStores { _, error in
            if error != nil {
                fatalError("Could not load persistent store")
            }
        }
        return container
    }()
    
    private func citiesViewModel() -> CitiesViewModel {
        return CitiesViewModel(
            cities: Constants.cities,
            networker: networker,
            cache: cache
        )
    }
    
    private(set) lazy var cache: Cache = CoreDataCache(persistentContainer: self.persistentContainer)
    
}

//
//  CitiesViewModel.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 10/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

enum Result<T, E> {
    case success(T)
    case failure(E)
}

struct City {
    let name: String
}

enum LoadCityError {
    case networkError(Error)
    case parserError
}

typealias LoadCityResult = Result<CityDetailViewModel, LoadCityError>

private enum WeatherDataOrigin {
    case server
    case cache
}

struct CitiesViewModel {
    
    let cities: [City]
    private let networker: Networker
    private let cache: Cache
    private let weatherInfoParser = WeatherInfoParser()
    private let urlCreator = UrlCreator()
    
    init(cities: [City], networker: Networker, cache: Cache) {
        self.cities = cities
        self.networker = networker
        self.cache = cache
    }
    
    func load(city: City, completion: @escaping (LoadCityResult) -> Void) {
        if let data = cache.cachedData(for: city, currentDate: Date()) {
            handleRequestSuccess(city: city, data: data, dataOrigin: .cache, completion: completion)
            return 
        }
        
        networker.sendGetRequest(url: urlCreator.url(city: city)) { result in
            switch result {
            case .success(let data, _):
                self.handleRequestSuccess(
                    city: city,
                    data: data,
                    dataOrigin: .server,
                    completion: completion
                )
            case .failure(let error):
                completion(.failure(.networkError(error)))
            }
        }
    }
    
    private func handleRequestSuccess(city: City, data: Data, dataOrigin: WeatherDataOrigin, completion: (LoadCityResult) -> Void) {
        switch weatherInfoParser.parse(data: data) {
        case .success(let weatherInfo):
            let detailViewModel = CityDetailViewModel(city: city, weatherInfo: weatherInfo)
            if dataOrigin == .server {
                cache.store(city: city, data: data, date: Date())
            }
            completion(.success(detailViewModel))
        case .failure:
            completion(.failure(.parserError))
        }
    }

}

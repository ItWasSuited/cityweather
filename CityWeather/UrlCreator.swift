//
//  UrlCreator.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

struct UrlCreator {
    
    func url(city: City) -> URL {
        var components = URLComponents(
            url: Constants.openWeatherUrl,
            resolvingAgainstBaseURL: false
        )!
        
        components.queryItems = [
            URLQueryItem(name: "q", value: city.name.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)),
            URLQueryItem(name: "appid", value: Constants.apiKey),
            URLQueryItem(name: "units", value: "metric"),
        ]
        
        return components.url!
    }
}

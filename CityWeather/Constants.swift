//
//  Constants.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

struct Constants {
    static let apiKey = "c555f8a0f8e20834176caf5f8c8c0aa0"
    static let openWeatherUrl = URL(string: "http://api.openweathermap.org/data/2.5/weather")!
    static let cities = [
        City(name: "Bilbao"),
        City(name: "Prague"),
        City(name: "Madrid"),
        City(name: "London"),
        City(name: "Buenos Aires"),
        City(name: "Rio de Janeiro"),
        City(name: "Krakow"),
        City(name: "Berlin")
    ]
    static let weatherCacheModelName = "WeatherCache"
    static let cacheTime = TimeInterval(10)
}

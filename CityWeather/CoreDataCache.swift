//
//  CoreDataCache.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import CoreData

protocol Cache {
    
    func cachedData(for city: City, currentDate: Date) -> Data?
    
    func store(city: City, data: Data, date: Date)
}

struct CoreDataCache: Cache {
    
    private let persistentContainer: NSPersistentContainer
    
    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }
    
    func cachedData(for city: City, currentDate: Date) -> Data? {
        log.info("Requesting cached data for city: \(city) date: \(currentDate)")
        
        var data: Data?
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest<CachedResponse> = CachedResponse.fetchRequest()
            let oldestDate = (currentDate - Constants.cacheTime) as NSDate
            let datePredicate = NSPredicate(format: "date > %@", oldestDate)
            let cityNamePredicate = NSPredicate(format: "cityName == %@", city.name)
            request.predicate = NSCompoundPredicate(
                andPredicateWithSubpredicates: [datePredicate, cityNamePredicate]
            )
            let result = try? context.fetch(request)
            data = result?.first?.data as? Data
        }
        
        log.info("Data for city: \(city) = \(data)")
        
        return data
    }
    
    func store(city: City, data: Data, date: Date) {
        log.info("Storing data for city: \(city) date:\(date)")
        
        let context = persistentContainer.newBackgroundContext()
        context.performAndWait {
            let request: NSFetchRequest<CachedResponse> = CachedResponse.fetchRequest()
            let result = try? context.fetch(request)
            
            let cachedResponse: CachedResponse
            if result?.first != nil {
                cachedResponse = result!.first!
                log.info("Found a cached response to update for city: \(city)")
            } else {
                cachedResponse = CachedResponse(context: context)
                log.info("Created new cached response for city: \(city)")
            }
            
            cachedResponse.cityName = city.name
            cachedResponse.data = data as NSData
            cachedResponse.date = date as NSDate
            
            try? context.save()
        }
    }
}

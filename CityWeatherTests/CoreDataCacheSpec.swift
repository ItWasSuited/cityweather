//
//  CoreDataCacheSpec.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import CoreData
import Quick
import Nimble
@testable import CityWeather

class CoreDataCacheSpec: QuickSpec {
    
    override func spec() {
        var sut: Cache!
        var persistentContainer: NSPersistentContainer!
        
        beforeEach {
            persistentContainer = NSPersistentContainer(name: Constants.weatherCacheModelName)
            
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            persistentContainer.persistentStoreDescriptions = [description]
            
            persistentContainer.loadPersistentStores { _, error in
                expect(error).to(beNil())
            }
            sut = CoreDataCache(persistentContainer: persistentContainer)
        }
        
        afterEach {
            sut = nil
            persistentContainer = nil
        }
        
        let city = City(name: "testCity")
        let data = "someData".data(using: .utf8)!
        let date = Date()
        
        describe("store") {
            
            beforeEach {
                sut.store(city: city, data: data, date: date)
            }
            
            it("creates a record if the cache is empty") {
                let context = persistentContainer.viewContext
                
                let request: NSFetchRequest<CachedResponse> = CachedResponse.fetchRequest()
                let result = try? context.fetch(request)
                expect(result).notTo(beNil())
                expect(result?.count) == 1
                
                guard let cachedResponse = result?.first else {
                    fail()
                    return
                }
                
                expect(cachedResponse.cityName) == "testCity"
                expect(cachedResponse.data) == data as NSData
                expect(cachedResponse.date) == date as NSDate
            }
            
            context("when is called for second time with the same city") {
                let newData = "newData".data(using: .utf8)!
                let newDate = Date()
                
                beforeEach {
                    sut.store(city: city, data: newData, date: newDate)
                }
                
                it("does not create a new record but updates it") {
                    let context = persistentContainer.viewContext
                    
                    let request: NSFetchRequest<CachedResponse> = CachedResponse.fetchRequest()
                    let result = try? context.fetch(request)
                    expect(result).notTo(beNil())
                    expect(result?.count) == 1
                    
                    guard let cachedResponse = result?.first else {
                        fail()
                        return
                    }
                    
                    expect(cachedResponse.cityName) == "testCity"
                    expect(cachedResponse.data) == newData as NSData
                    expect(cachedResponse.date) == newDate as NSDate
                }
            }
        }
        
        describe("cachedData") {
            
            context("when there is a record for the given city") {
                var cachedData: Data?
                
                beforeEach {
                    sut.store(city: city, data: data, date: date)
                }
                
                context("and the difference between the current time and the record time is smaller than the cacheTime") {
                    
                    beforeEach() {
                        cachedData = sut.cachedData(
                            for: city,
                            currentDate: date + (Constants.cacheTime / 2)
                        )
                    }
                    
                    it("returns that data") {
                        expect(cachedData) == data
                    }
                }
                
                context("and the difference between the current time and the record time is bigger than the cacheTime") {
                    
                    beforeEach() {
                        cachedData = sut.cachedData(
                            for: city,
                            currentDate: date + (Constants.cacheTime * 2)
                        )
                    }
                    
                    it("returns nil") {
                        expect(cachedData).to(beNil())
                    }
                }
            }
            
            context("when there is a record for a different city") {
                var cachedData: Data?
                
                beforeEach {
                    sut.store(city: City(name: "otherCity"), data: data, date: date)
                }
                
                context("and the difference between the current time and the record time is smaller than the cacheTime") {
                    
                    beforeEach() {
                        cachedData = sut.cachedData(
                            for: city,
                            currentDate: date + (Constants.cacheTime / 2)
                        )
                    }
                    
                    it("returns nil") {
                        expect(cachedData).to(beNil())
                    }
                }
            }
        }
    }
    
}

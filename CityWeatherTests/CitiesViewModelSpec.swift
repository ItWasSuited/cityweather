//
//  CitiesViewModelSpec.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 10/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import CityWeather

class CitiesViewModelSpec: QuickSpec {
    
    override func spec() {
        
        var sut: CitiesViewModel!
        
        var existingCity: City!
        var networker: FakeNetworker!
        var cache: FakeCache!
        
        beforeEach {
            existingCity = City(name: "testCity")
            networker = FakeNetworker()
            cache = FakeCache()
            
            sut = CitiesViewModel(
                cities: [existingCity],
                networker: networker,
                cache: cache
            )
        }
        
        afterEach {
            sut = nil
            
            existingCity = nil
            networker = nil
            cache = nil
        }
        
        describe("loadCity") {
            
            context("when there is no cached data and the networker succeeds") {
                beforeEach {
                    cache.data = nil
                    networker.result = .success(JsonHelper.completeData, 200)
                }
                
                it("calls the completion with the data from the networker") {
                    var maybeResult: LoadCityResult? = nil
                    sut.load(city: existingCity) {
                        maybeResult = $0
                    }
                    
                    guard
                        let result = maybeResult,
                        case .success(let cityDetailViewModel) = result
                    else {
                        fail()
                        return
                    }
                    
                    expect(cityDetailViewModel.city.name) == existingCity.name
                    
                    let weatherInfo = cityDetailViewModel.weatherInfo
                    expect(weatherInfo.imageUrl) == URL(string: "http://openweathermap.org/img/w/50d.png")
                    expect(weatherInfo.temperature) == Measurement<UnitTemperature>(value: 5.39, unit: .celsius)
                    expect(weatherInfo.main) == "Fog"
                    expect(weatherInfo.description) == "fog"
                    expect(weatherInfo.humidity) == 100
                    expect(weatherInfo.wind?.speed) == Measurement<UnitSpeed>(value: 3.1, unit: .metersPerSecond)
                    expect(weatherInfo.wind?.direction) == WindDirection.s
                }
                
                it("stores the data in the cache") {
                    sut.load(city: existingCity) { _ in }
                    
                    expect(cache.storedData) == JsonHelper.completeData
                }
            }
            
            context("when there is valid cached weather data for the given city") {
                beforeEach {
                    cache.data = JsonHelper.completeData
                }
                
                it("calls the completion with the cached data") {
                    var maybeResult: LoadCityResult? = nil
                    sut.load(city: existingCity) {
                        maybeResult = $0
                    }
                    
                    guard
                        let result = maybeResult,
                        case .success(let cityDetailViewModel) = result
                        else {
                            fail()
                            return
                    }
                    
                    expect(cityDetailViewModel.city.name) == existingCity.name
                    
                    let weatherInfo = cityDetailViewModel.weatherInfo
                    expect(weatherInfo.imageUrl) == URL(string: "http://openweathermap.org/img/w/50d.png")
                    expect(weatherInfo.temperature) == Measurement<UnitTemperature>(value: 5.39, unit: .celsius)
                    expect(weatherInfo.main) == "Fog"
                    expect(weatherInfo.description) == "fog"
                    expect(weatherInfo.humidity) == 100
                    expect(weatherInfo.wind?.speed) == Measurement<UnitSpeed>(value: 3.1, unit: .metersPerSecond)
                    expect(weatherInfo.wind?.direction) == WindDirection.s
                }
                
                it("does not call the networker") {
                    sut.load(city: existingCity) { _ in }
                    
                    expect(networker.sendGetRequestCalled).to(beFalsy())
                }
                
                it("does not store the data in the cache") {
                    sut.load(city: existingCity) { _ in }
                    
                    expect(cache.storedData).to(beNil())
                }
                
            }
            
            context("when the networker fails") {
                let networkerError = NSError(domain: "testDomain", code: 0, userInfo: nil)
                beforeEach {
                    networker.result = .failure(networkerError)
                }
                
                it("calls the given completion with the expected error") {
                    var maybeResult: LoadCityResult? = nil
                    sut.load(city: existingCity) {
                        maybeResult = $0
                    }
                    
                    guard
                        let result = maybeResult,
                        case .failure(let loadCityError) = result
                    else {
                        fail()
                        return
                    }
                    
                    guard case .networkError(let underlyingError) = loadCityError else {
                        fail()
                        return
                    }
                    
                    expect(underlyingError as NSError) == networkerError
                }
                
            }
            
            context("when invalid json is received") {
                beforeEach {
                    networker.result = .success(JsonHelper.invalidData, 200)
                }
                
                it("calls the given completion with the expected error") {
                    var maybeResult: LoadCityResult? = nil
                    sut.load(city: existingCity) {
                        maybeResult = $0
                    }
                    
                    guard
                        let result = maybeResult,
                        case .failure(let loadCityError) = result
                    else {
                        fail()
                        return
                    }
                    
                    guard case .parserError() = loadCityError else {
                        fail()
                        return
                    }
                }
            }
            
            context("when unexpected json is received") {
                beforeEach {
                    networker.result = .success(JsonHelper.unexpectedData, 200)
                }
                
                it("calls the given completion with the expected error") {
                    var maybeResult: LoadCityResult? = nil
                    sut.load(city: existingCity) {
                        maybeResult = $0
                    }
                    
                    guard
                        let result = maybeResult,
                        case .failure(let loadCityError) = result
                    else {
                            fail()
                            return
                    }
                    
                    guard case .parserError() = loadCityError else {
                        fail()
                        return
                    }
                }
            }
            
        }
    }
    
}

class FakeCache: Cache {
    
    var data: Data?
    
    func cachedData(for city: City, currentDate: Date) -> Data? {
        return data
    }
    
    var storedData: Data?
    
    func store(city: City, data: Data, date: Date) {
        storedData = data
    }
}

//
//  UrlCreatorSpec.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import CityWeather

class UrlCreatorSpec: QuickSpec {
    
    override func spec() {
        describe("the url") {

            it("contains the expected params") {
                let city = City(name: "testName")
                
                let url = UrlCreator().url(city: city)
                let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
                let queryItems = urlComponents.queryItems
                
                expect(queryItems).notTo(beNil())
                expect(queryItems?.count) == 3
                expect(queryItems).to(contain(URLQueryItem(name: "q", value: "testName")))
                expect(queryItems).to(contain(URLQueryItem(name: "appid", value: Constants.apiKey)))
                expect(queryItems).to(contain(URLQueryItem(name: "units", value: "metric")))
            }
            
            it("when the city name contains spaces URL encodes it") {
                let city = City(name: "test name")
                
                let url = UrlCreator().url(city: city)
                let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
                let queryItems = urlComponents.queryItems
                
                expect(queryItems).to(contain(URLQueryItem(name: "q", value: "test%20name")))
            }
            
        }
    }
    
}

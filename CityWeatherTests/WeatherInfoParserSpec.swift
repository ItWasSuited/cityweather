//
//  WeatherInfoParserSpec.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import CityWeather

class WeatherInfoParserSpec: QuickSpec {
    
    override func spec() {
        
        var sut: WeatherInfoParser!
        
        beforeEach {
            sut = WeatherInfoParser()
        }
        
        afterEach {
            sut = nil
        }
        
        describe("parse data") {
            
            context("when the wind is missing in the json") {
                
                it("returns nil wind") {
                    let result = sut.parse(data: JsonHelper.missingWindData)
                    guard case .success(let weatherInfo) = result else {
                        fail()
                        return
                    }
                    
                    expect(weatherInfo.imageUrl) == URL(string: "http://openweathermap.org/img/w/50d.png")
                    expect(weatherInfo.temperature) == Measurement<UnitTemperature>(value: 5.39, unit: .celsius)
                    expect(weatherInfo.main) == "Fog"
                    expect(weatherInfo.description) == "fog"
                    expect(weatherInfo.humidity) == 100
                    expect(weatherInfo.wind).to(beNil())
                }
            }
        }
    }
    
}

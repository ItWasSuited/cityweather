//
//  FakeNetworker.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 10/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation
@testable import CityWeather

class FakeNetworker: Networker {
    
    var result: NetworkerResult?
    var sendGetRequestCalled = false
    
    func sendGetRequest(url: URL, completion: @escaping (NetworkerResult) -> Void) {
        sendGetRequestCalled = true
        if let result = result {
            completion(result)
        }
    }
    
}

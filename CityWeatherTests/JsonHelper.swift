//
//  JsonHelper.swift
//  CityWeather
//
//  Created by Emilio Fernandez Astigarraga on 11/12/2016.
//  Copyright © 2016 Emilio Fernandez Astigarraga. All rights reserved.
//

import Foundation

class JsonHelper {
    
    static var completeData: Data {
        let bundle = Bundle(for: self)
        let url = bundle.url(forResource: "completeResponse.json", withExtension: nil)!
        return try! Data(contentsOf: url)
    }
    
    static var invalidData: Data {
        return "jshbfjhsd".data(using: .utf8)!
    }
    
    static var unexpectedData: Data {
        return "[{\"aa\":22}]".data(using: .utf8)!
    }
    
    static var missingWindData: Data {
        let bundle = Bundle(for: self)
        let url = bundle.url(forResource: "missingWind.json", withExtension: nil)!
        return try! Data(contentsOf: url)
    }
    
}
